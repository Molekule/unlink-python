import csv
import time
import boto3
from boto3.dynamodb.conditions import Key, Attr
from botocore.exceptions import ClientError
import requests
import json
import awscli

endpoint = "https://api-v1.molekule.com"
data = {"email":"admin3@molekule.com","password":"RgM0kT5&1A"}
headers = {'content-type': 'application/json'}
response = requests.post(endpoint + "/write/api/v1/admin/login", data=json.dumps(data), headers=headers)
token = json.loads(response.text)["data"]["token"]
client = boto3.client(
    'iot-data',
    aws_access_key_id='AKIAIAKVMND5QSJLDYAQ',
    aws_secret_access_key='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',
    region_name='us-west-2'
)

def checkSerialNumber(token, searchSN):
    query = {"pageSize": 512,
             "pageNo": 0,
             "searchString": '',
             "sortOrder": 1
             }

    response = requests.get(
        endpoint + "/read/api/v1/list/devices",
        params=query,
        headers={'token': token})

    data = json.loads(response.text)["data"]

    devices = []
    while (data['count'] != 0):

        for device in data['devices']:
            if device['serialNumber'] == searchSN:
                return device
def writeNewFile(sn):
    with open('cleanbatch.csv', 'w') as fw:
        writer = csv.writer(fw)
        writer.writerows(sn)

def filterFile():
    print("Processing batch file...")
    with open('batch.csv', 'r') as f:
        reader = csv.reader(f)
        for row in reader:
            print(row)
            serialNumber = row[0]
            if checkSerialNumber(serialNumber):
                writeNewFile(serialNumber)
            else:
                print("Molekule SN not found: " + serialNumber)
    print("Processing ended.")


def getDeviceFromList(token, searchSN):
    query = {"pageSize": 512,
             "pageNo": 0,
             "searchString": '',
             "sortOrder": 1
             }

    response = requests.get(
        endpoint + "/read/api/v1/list/devices",
        params=query,
        headers={'token': token})

    data = json.loads(response.text)["data"]

    devices = []
    while (data['count'] != 0):

        for device in data['devices']:
            if device['serialNumber'] == searchSN:
                return device

        query['pageNo'] += 1
        devices += data['devices']
        response = requests.get(
            endpoint + "/read/api/v1/list/devices",
            params=query,
            headers={'token': token})
        data = json.loads(response.text)["data"]
        print('.', end='')

    return devices


def unlinkUser(token, serialNumber, encryptedUserId):
    response = requests.delete(
        endpoint + "/write/api/v1/unlink/device",
        json={'serialNumber': serialNumber, 'encryptedUserId': encryptedUserId},
        headers={'token': token, 'content-type': 'application/json'})
    print(response)

def update_shadow(sn):
    response = client.update_thing_shadow(
        thingName=sn,
        payload=json.dumps({
            'state': {
                'desired': {
                    'sr': 0
                }
            }
        })
    )

#
# Filter the batch list to remove un-existent Molekules
#

#filterFile()

file = open('batch.csv', "r")
read = csv.reader(file)
for row in read:
    serialNumber = row[0]
    print('Unlinking users from ' + serialNumber)

    device = getDeviceFromList(token, serialNumber)

    for user in device['users']:
        unlinkUser(token, device['serialNumber'], user['encryptedUserId'])
        print(update_shadow(serialNumber))



