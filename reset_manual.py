import csv
import boto3
from boto3.dynamodb.conditions import Key, Attr
from botocore.exceptions import ClientError
import logging
import time
import datetime
import json
import requests
import awscli
from settings import get_env_variable, current_time, DecimalEncoder

dynamodb = boto3.resource('dynamodb', region_name='us-west-2',
                          endpoint_url="https://dynamodb.us-west-2.amazonaws.com",
                          aws_access_key_id='AKIAIAKVMND5QSJLDYAQ',
                          aws_secret_access_key='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')
table = dynamodb.Table('FilterPeformance-Prod')

client = boto3.client(
    'iot-data',
    aws_access_key_id='AKIAIAKVMND5QSJLDYAQ',
    aws_secret_access_key='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',
    region_name='us-west-2'
)

algo_version = '0.10'
prefilter_lifetime = int(129600)
nanofilter_lifetime = int(388800)

def update_shadow(sn):
    new_payload = {'state': {'desired': {'ph': 99, 'mh': 99}}}

    response = client.publish(
        topic='$aws/things/' + sn + '/shadow/update',
        qos=1,
        payload=json.dumps(new_payload, cls=DecimalEncoder)
    )

    if response['ResponseMetadata']['HTTPStatusCode'] == 200:
        return {"status": "Success"}
    else:
        logging.error(response)
        return {"status": "Fail"}

def set_state(sn):
    # First update shadow
    response = update_shadow(sn)
    if response['status'] is 'Fail':
        logging.error("Failed to save shadow for device " + sn)
        return {'status': 'Fail'}

def put_item(table, item, allow_overwrite=False):
    try:
        if not allow_overwrite:
            response = table.put_item(
                Item=item,
                # conditional expression here keeps us from overwriting items
                ConditionExpression='attribute_not_exists(serialNumber)'
            )
        else:
            response = table.put_item(
                Item=item,
            )
            print (response)

    except ClientError as e:
        print (e.response)
        if e.response['Error']['Message'] == "The conditional request failed":
            logging.error("put_item ran into overwrite condition (serialNumber, createdAt pair already exist)")
        else:
            logging.error(e)
        return {'status': 'Fail'}

    if response['ResponseMetadata']['HTTPStatusCode'] == 200:
        return {'status': 'Success'}
    else:
        return {'status': 'Fail'}


def save_state(table, state):
    state['createdAt'] = current_time()
    return put_item(table, state)

def setInitialState(serial_number):
    initial_state = {
        'serialNumber': serial_number,
        'createdAt': int(time.mktime(datetime.datetime.now().timetuple())) * 1000,
        'prefilter_lifetime': prefilter_lifetime,
        'prefilter_lifetime_remaining': prefilter_lifetime,
        'prefilter_health': 99,

        'nanofilter_lifetime': nanofilter_lifetime,
        'nanofilter_lifetime_remaining': nanofilter_lifetime,
        'nanofilter_health': 99,

        'algo_version': algo_version,
        'static': {algo_version: {'fan_time_offset_pf': 0, 'fan_time_offset_nf': 0, 'fan_time': 0}},
        'itemType': 'state',
        'reset_by': 'BStockReset'
    }
    ret = save_state(table, initial_state)
    if ret['status'] == 'Success':
        ret['state'] = initial_state
        return ret
    return ret
#
#Reads from CSV and resets rows
#
file = open('batch.csv', "r")
read = csv.reader(file)
for row in read:
    serialNumber = row[0]
    print('Resetting to initial state: ' + serialNumber)
    setInitialState(serialNumber)
    set_state(serialNumber)


